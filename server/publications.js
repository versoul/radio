Meteor.publish('radios', function() {
    return Radios.find();
});
Meteor.publish('radioGroups', function() {
    return RadioGroups.find();
});
Meteor.publish('genres', function() {
    return Genres.find();
});
Meteor.publish('userList', function (){
  return Meteor.users.find({});
});
Meteor.publish('messages', function (radio){
  var u = Meteor.users.findOne(this.userId);
  if(u){

    var r = u.profile.curRadio;
    if(r){
      var ret = [];
      var msgs = Messages.find({$or: [{radio: r}, {radio: 0}]}, {sort: {date: -1}}).fetch();
      var date = Date.now();
      _.each(msgs, function(val){
        if(date - val.date > 1000 * 2){
          Messages.remove(val._id);
        }
        else{
          ret.push(val);
        }
      });
      return Messages.find({$or: [{radio: r}, {radio: 0}], date: {$gt: date - 2000} }, {sort: {date: -1}, limit: 15});
    }
  }
  else{
    return [];
  }

});
Meteor.publish('disconnect', function (guid){
  console.log('connect WID', guid);
  var res = Disconnect.findOne({"_id": guid});
  if(!res){
    console.log('create new rec');
    Disconnect.insert({
        '_id' : guid,
        'radio': null
    });
  }

  this._session.socket.on('close', Meteor.bindEnvironment(function() {
		console.log('disconnect WID', guid);
    var res = Disconnect.findOne({"_id": guid});
    console.log('res', res)
    if(res){
      Radios.update(res.radio, {$inc: {'listen': -1}});
      Disconnect.remove(guid);
    }

	}));

	return Disconnect.find({});
});
