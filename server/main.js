import { Meteor } from 'meteor/meteor';
import { Session } from 'meteor/session';
var internetradio = Meteor.npmRequire('node-internet-radio');

Meteor.startup(() => {
  // code to run on server at startup
});

/*Meteor.users.find({ "status.online": true }).observe({
  added: function(u) {
    console.log('online', u.username)
  },
  removed: function(u) {
    console.log('offline', u.username)
  }
});*/

/*UserPresence.onSessionConnected(function(connection){
    console.log('con', connection);
});
UserPresence.onSessionDisconnected(function(connection){
    console.log('discon', connection)
});*/

/*Meteor.setInterval(function () {
  var now = (new Date()).getTime();
  RadioOnline.find().forEach(function (r) {
    if(r.last_seen < now -2 * 1000){
      if(r.id){
        Radios.update(r.id, {$inc: {'listen': -1}});
      }
      RadioOnline.remove(r._id);
    }
    else if(!r.last_seen){
        RadioOnline.remove(r._id);
    }



  });
}, 1000);*/

var lastTitle = null;
function getAlbum(radioId, title){
  if(title && title != lastTitle){
    var res = Meteor.http.get("http://www.shazam.com/fragment/search/"+encodeURI(title), {followRedirects:true, headers: {
      "Accept":"application/json, text/javascript, */*; q=0.01",
      "Host":"www.shazam.com",
      "Referer":"http://www.shazam.com/ru",
      "User-Agent":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36",
      "Cookie":"wid=662731c8-77c3-4594-a20c-b49b3bfa12f7; raygun4js-userid=813d076d-df85-c908-e573-baf98d955181; fbm_210827375150=base_domain=.shazam.com; fbsr_210827375150=_ECCwbDJCELDhjyprfawrKwDu40KEftPOdNNCMYi3uQ.eyJhbGdvcml0aG0iOiJITUFDLVNIQTI1NiIsImNvZGUiOiJBUURfYUZQZlh2VlRsUklMRm9nR3FjWWx3LXpwMEktVnIyMnV4VWxNOVU5NnVYWEVjYWllY0E1RXlBdGtqN2NBSjFlSFd0bE5kT1FGU2x1eVZTUGpCZTNIZHB6WnlKb3ZIUHdGU3RYYmF1Z04tOS1DWWc1bklUZWpMdG1NUzJvTDVTYktlcHRfTHJEbzA1WHExSlladjVscHgwcW0xenJ4Z0p6TEFaOEJXQnVkdC1LZFZpZ18yWE1yWWNxbWJXdkkxaUM3UzNZYjFIWEktSEVoQ3ltdEd0SHlEU0lwQzdpUXVMNFBnWmZjUHRFSUpHU0Z4YjdFdG1QSUJJTzQ5SzhLWi1nc3JTcll2UTJhMG5icTdEY2x6Y1FhRU4xX1JLMEVYaE44UmQ0dngyc3htQmpOVVUzZG9Tbks0ODQzM1B4bEtwS1hvcWdRblhzQUtDUjVQbHl4MGNzUyIsImlzc3VlZF9hdCI6MTQ3MjEzNDE5MiwidXNlcl9pZCI6Ijg3MzMyNzgxNjA2MzkyMiJ9; registration=2-HvaCCSBTSwDlPz7TTQaxCGC58Cy4L6xRlt9IzCF9PX%2BTDhrCsvxQ5BCBpdj6f0c5wD36Ypi3VRng4lZSLa1%2BKMBHl0xYrVZ64EwSxBKFU1ack8uNDRMUprSZh0vEfqzPGGQX21jaEiYTl8LnY5y3ttQ23hJd5Qhy1dz6jb7MAhiu7GndGnpWpQcvaxVv3XRlZvKuCfTS73y3%2Fijw3CfR7N5d5P00aOqfSXmQqLcdZ92csKG43Bu%2F5zEz2gzH%2Fvl9hV2tg%2FcqCLgUIi3pnhdvetL6LkarWeFJ9g36ToAAH2bURWSm; __utma=184174564.1799188052.1472132211.1472132211.1472132211.1; __utmb=184174564.58.8.1472135431113; __utmc=184174564; __utmz=184174564.1472132211.1.1.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=(not%20provided)"
    }}, function(err, result) {
      if (!err){
        //console.log('answ', result.data.tracksresult.tracks[0])
        if(result.data && result.data.tracksresult.tracks.length){
          Radios.update(radioId, {$set: {'info.album':result.data.tracksresult.tracks[0].image400}});
        }
        else{
          Radios.update(radioId, {$set: {'info.album':'img/album.jpg'}});
        }

      }
      else{
        console.log('err', err)
      }
        // do something with the result.
    });
    lastTitle = title;
  }
}
Meteor.methods({
    /*keepalive: function (id, userId) {
      if (!RadioOnline.findOne({id: id, userId: userId})){
        RadioOnline.insert({id: id, userId: userId});
      }
      RadioOnline.update({id: id}, {$set: {last_seen: (new Date()).getTime()}});
    },*/
    getRadioInfo: function (id, url) {
      internetradio.getStationInfo(url, Meteor.bindEnvironment(function(error, station) {
        if(station && station.title){
          Radios.update(id, {$set: {'info.title':station.title}});
          getAlbum(id, station.title);
        }
      }), internetradio.StreamSource.STREAM);
    },
    sendMessage: function(data){
      data.date = Date.now();
      Messages.insert(data);
      console.log('bb', Messages.find().fetch())
    },
    likeRadio: function (id) {
      Radios.update(id, {$inc: {'like':1}});
    },
    incRadioListeners: function (oldId, newId) {
      Radios.update(oldId, {$inc: {'listen': -1}});
      Radios.update(newId, {$inc: {'listen': 1}});
    },
    addEditGenre: function (genre) {
      var id = genre._id;
      delete genre._id;
      if(id != ''){
        Genres.update(id, {$set: genre});
      }
      else{
        Genres.insert(genre);
      }
    },
    delGenre: function (id) {
      Genres.remove(id);
    },
    addEditRadio: function (radio) {
      var id = radio._id;
      delete radio._id;
      if(id != ''){
        Radios.update(id, {$set: radio});
      }
      else{
        radio.listen = 0;
        radio.like = 0;
        Radios.insert(radio);
      }
    },
    delRadio: function (id) {
      Radios.remove(id);
    },
    cleareDopinfo: function () {
      Radios.update({}, {$set: {listen:0, like:0}}, {multi: true});
      RadioOnline.remove({});
      Disconnect.remove({});

    },
    cleareRadios: function () {
      Radios.remove({});
      Genres.remove({});
      Disconnect.remove({});
      Meteor.users.remove({});
    },
    initRadios: function () {
      if (Radios.find({}).count() === 0) {
            Radios.insert({
                'name' : 'rouge-lounge',
                'url': 'http://rouge-lounge.ice.infomaniak.ch:80/rouge-lounge-64.aac',
                'genre': 'SMxcS32p8h3GsNENB',
                'listen': 0,
                'like':0,
                'messages':[],
                'description': 'rouge-lounge'
            });
            Radios.insert({
                'name' : 'wr-ouftivi.ice.infomaniak.ch',
                'url': 'http://wr-ouftivi.ice.infomaniak.ch:80/wr-ouftivi-128',
                'genre': 'SMxcS32p8h3GsNENB',
                'listen': 0,
                'like':0,
                'messages':[],
                'description': 'wr-ouftivi.ice.infomaniak.ch'
            });
            Radios.insert({
                'name' : 'tonymannradio',
                'url': 'http://tonymannradio.serverroom.us:6314/;?cb=186242.mp3',
                'genre': 'SMxcS32p8h3GsNENB',
                'listen': 0,
                'like':0,
                'messages':[],
                'description': 'tonymannradio'
            });
            Radios.insert({
                'name' : 'vibration.stream',
                'url': 'http://91.121.38.100:8220/;stream/1',
                'genre': 'SMxcS32p8h3GsNENB',
                'listen': 0,
                'like':0,
                'messages':[],
                'description': 'vibration.stream'
            });
            Radios.insert({
                'name' : '99.198.118.250:8241',
                'url': 'http://99.198.118.250:8241/stream',
                'genre': 'SMxcS32p8h3GsNENB',
                'listen': 0,
                'like':0,
                'messages':[],
                'description': '99.198.118.250:8241'
            });
            Radios.insert({
                'name' : '64.150.176.42:8295',
                'url': 'http://64.150.176.42:8295/stream',
                'genre': 'SMxcS32p8h3GsNENB',
                'listen': 0,
                'like':0,
                'messages':[],
                'description': '64.150.176.42:8295'
            });
        }
        if (Genres.find({}).count() === 0) {
          Genres.insert({
              'name' : 'hip-hop1',
              'icon': '1'
          });
          Genres.insert({
              'name' : 'hip-hop2',
              'icon': '2'
          });
          Genres.insert({
              'name' : 'hip-hop3',
              'icon': '3'
          });
          Genres.insert({
              'name' : 'hip-hop4',
              'icon': '4'
          });
          Genres.insert({
              'name' : 'hip-hop5',
              'icon': '5'
          });
          Genres.insert({
              'name' : 'hip-hop6',
              'icon': '6'
          });
          Genres.insert({
              'name' : 'hip-hop7',
              'icon': '7'
          });
          Genres.insert({
              'name' : 'hip-hop8',
              'icon': '8'
          });
          Genres.insert({
              'name' : 'hip-hop9',
              'icon': '9'
          });
          Genres.insert({
              'name' : 'hip-hop10',
              'icon': '10'
          });
          Genres.insert({
              'name' : 'hip-hop11',
              'icon': '11'
          });
          Genres.insert({
              'name' : 'hip-hop12',
              'icon': '12'
          });
          Genres.insert({
              'name' : 'hip-hop13',
              'icon': '13'
          });
          Genres.insert({
              'name' : 'hip-hop14',
              'icon': '14'
          });
          Genres.insert({
              'name' : 'hip-hop15',
              'icon': '15'
          });
          Genres.insert({
              'name' : 'hip-hop16',
              'icon': '16'
          });
          Genres.insert({
              'name' : 'hip-hop17',
              'icon': '17'
          });
          Genres.insert({
              'name' : 'hip-hop18',
              'icon': '18'
          });
          Genres.insert({
              'name' : 'hip-hop19',
              'icon': '19'
          });
          Genres.insert({
              'name' : 'hip-hop20',
              'icon': '20'
          });
          Genres.insert({
              'name' : 'hip-hop21',
              'icon': '21'
          });
          Genres.insert({
              'name' : 'hip-hop22',
              'icon': '22'
          });
          Genres.insert({
              'name' : 'hip-hop23',
              'icon': '23'
          });
          Genres.insert({
              'name' : 'hip-hop24',
              'icon': '24'
          });
        }
    }
});
