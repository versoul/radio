import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

Meteor.subscribe('radios');
Meteor.subscribe('genres');
Meteor.subscribe('radioGroups');
Meteor.subscribe('userList');

//Meteor.subscribe('messages');
//переходим в режим телефона
//Meteor.Device.preferPhone();

(jQuery, window, document), $(document).ready(function() {

    $(".player-next").click(function() {
    	var station = "station-";
    	var activeArt = $(".bro-mustype--active").attr('id');
    	$(".bro-mustype--active").removeClass("bro-mustype--active");
    	//activeArt.removeClass("bro-mustype--active").addClass("bro-mustype");
    	//activeArt.parent().parent().next(".bro-mustype").addClass("111111111");
    	//activeArt.closest("div").closest("div").find(".bro-mustype").addClass("bro-mustype bro-mustype--active");
    	var numberStation = activeArt.substring(8, 9);
    	if (numberStation < 8) {
    		numberStation ++;
    	}
    	else {
    		numberStation = 1;
    	}

    	station = station + numberStation;
    	$("#"+station).addClass("bro-mustype--active");

    	var nameStation = $("#"+station).children(".radio-name").html();

    	$("#name-active-station").html(nameStation);
    });

    $(".player-prew").click(function() {
    	var station = "station-";
    	var activeArt = $(".bro-mustype--active").attr('id');
    	$(".bro-mustype--active").removeClass("bro-mustype--active");
    	//activeArt.removeClass("bro-mustype--active").addClass("bro-mustype");
    	//activeArt.parent().parent().next(".bro-mustype").addClass("111111111");
    	//activeArt.closest("div").closest("div").find(".bro-mustype").addClass("bro-mustype bro-mustype--active");
    	var numberStation = activeArt.substring(8, 9);
    	if (numberStation > 1) {
    		numberStation --;
    	}
    	else {
    		numberStation = 8;
    	}

    	station = station + numberStation;
    	$("#"+station).addClass("bro-mustype--active");

    	var nameStation = $("#"+station).children(".radio-name").html();

    	$("#name-active-station").html(nameStation);
    });


    var firstScroll = false;
    var movedown = true;
    var timerId = setInterval(function() {
                        if(movedown) $(".strelochka").css("top", "80%");
                        else $(".strelochka").css("top", "75%");
                        movedown = !movedown;
                        if(firstScroll) clearTimeout(timerId);
                    }, 1100);




    $('body').on('click', '.cross', function(){
        number=$(this).closest(".message").index();
        for(var i=0;i<=number;i++){
            $(".message:nth-child("+i+")").animate({top:"+=37%"},500)}
        $(this).closest(".message").fadeOut(500,function(){
            $(this).remove();

        });

    });




});
