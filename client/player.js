Player = new function(){
  var _player = null;
  this.inited = false;

  this.init = function(){
    _player = $("#jquery_jplayer_1");
    _player.jPlayer({
      /*ready: function () {

      },*/
      cssSelectorAncestor: "#jp_container_1",
      swfPath: "/js",
      supplied: "m4a, oga, mp3",
      useStateClassSkin: true,
      autoBlur: false,
      smoothPlayBar: true,
      keyEnabled: true,
      remainingDuration: true,
      toggleDuration: true
    });

    _player.bind($.jPlayer.event.error, function(event) {
      $("html").removeClass("bro-blur"), $("#bro-modal2").removeClass("bro-modal--open");
      alert('Error');
      console.log(event)
    });
    _player.bind($.jPlayer.event.playing , function(event) {
      $("html").removeClass("bro-blur"), $("#bro-modal2").removeClass("bro-modal--open");
    });
    _player.bind($.jPlayer.event.loadstart, function(event) {
      $("html").addClass("bro-blur"), $("#bro-modal2").addClass("bro-modal--open");
    });

    this.inited = true;
  };

  this.play = function(url){
    if(!this.inited){
      this.init();
    }

    _player.jPlayer("setMedia",{
      m4a: url,
      oga: url,
      mp3: url
    }).jPlayer("play");
  };
  this.resume = function(){
    _player.jPlayer("play");
  };
  this.pause = function(){
    _player.jPlayer("pause");
  };
};
