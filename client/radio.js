Radio = new function(){
  var _this = this;
  this.curRadioId = new ReactiveVar(null);
  this.updateInfoInterval = null;
  this.history = [];
  this.curRadioLike = null;
  this.lastLike = {time:0, counter:0};
  //this.curRadioGenre = null;

  function getRandomArbitrary(min, max) {
      return Math.floor(Math.random() * (max - min)) + min;
  }
  this.getNextBest = function(genre){
    recountMax = true;
    this.curRadioGenre = genre;
    var u = Meteor.user();
    var radiosList = [];
    if( u.username.indexOf('guest-#') != 0 && u.profile.sortPopular == true){
      radiosList = Radios.find({'genre':genre}, {sort: {'like': -1, 'listen': -1}}).fetch();
    }
    else if( u.username.indexOf('guest-#') != 0 && u.profile.sortFavorite == true){
      r = Radios.find({'genre':genre}).fetch();
      radiosList = _.filter(r, function(val){
       if(Meteor.user().profile.favRadios.indexOf(val._id) !== -1 ){
         return true;
       }
     });
    }
    else{
      radiosList = Radios.find({'genre':genre}).fetch();
    }


    var r = null;
    var f = false;
    if(!_this.curRadioId.get()){
      r = radiosList[0];
    }
    else{
      r = _.find(radiosList, function(val){
        if(_this.history.indexOf(val._id) == -1 ){
          return true;
        }
      });
    }
    if(!r){
      r = radiosList[0];
      this.history = [];
    }


    this.curRadioId.set(r._id);
    this.curRadioLike = r.like;
    this.lastLike = {time:0, counter:0};
    this.history.push(this.curRadioId.get());
    Meteor.users.update({"_id": Meteor.userId()}, {$set: {"profile.curRadio": r._id}});
    Disconnect.update({"_id":window.wid}, {$set: {"radio": r._id}});
    return r;
  };
  this.getPrev = function(){
    recountMax = true;
    var cur = this.history.pop();
    var r = Radios.findOne(this.history[this.history.length-1]);
    if(!r){
      r = Radios.findOne(cur);
      this.history.push(cur);
    }
    this.curRadioId.set(r._id);
    this.curRadioLike = r.like;
    this.lastLike = {time:0, counter:0};
    Meteor.users.update({"_id": Meteor.userId()}, {$set: {"profile.curRadio": r._id}});
    Disconnect.update({"_id":window.wid}, {$set: {"radio": r._id}})
    return r;
  };
  this.getById = function(id){
    recountMax = true;
    var cur = this.history.pop();
    var r = Radios.findOne(id);
    if(!r){
      r = Radios.findOne(cur);
      this.history.push(cur);
    }
    this.curRadioId.set(r._id);
    this.curRadioLike = r.like;
    this.lastLike = {time:0, counter:0};
    Meteor.users.update({"_id": Meteor.userId()}, {$set: {"profile.curRadio": r._id}});
    Disconnect.update({"_id":window.wid}, {$set: {"radio": r._id}})
    return r;
  };
};
