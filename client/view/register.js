Template.register.helpers({
    tst: function () {
        return "tst";
    }
});

Template.register.onRendered(function () {
  var u = Meteor.user();
  console.log('reg onrender')
  if(u){
    console.log('if user')
      var r = u.profile.curRadio;
      if(r){
        console.log('ifradio')
        //Radios.update(r, {$inc: {'listen': -1}});
        Meteor.users.update({"_id": u._id}, {$set: {"profile.curRadio": null}});
        Radio.curRadioGenre = null;
        Radio.curRadioId.set(null);
        Radio.updateInfoInterval = null;
        Player.inited = false;
      }
  }
});

Template.register.events({
    'submit .register-form': function(e,t){
        e.preventDefault();
        var regData = {};
        regData.fullname = $('.register-form [name=fullname]').val();
        regData.email = $('.register-form [name=email]').val();
        regData.address = $('.register-form [name=address]').val();
        regData.city = $('.register-form [name=city]').val();
        regData.country = $('.register-form [name=country]').val();
        regData.username = $('.register-form [name=username]').val();
        regData.password = $('.register-form [name=password]').val();

        Accounts.createUser({
          username: regData.username,
          email: regData.email,
          password: regData.password,
          profile: {
            fullname: regData.fullname,
            country: regData.country,
            city: regData.city,
            address: regData.address,
            favGenres:[],
            favRadios:[],
            showMessages:true,
            sortPopular:false,
            sortFavorite:false
          }
        }, function(err){
          if(err){
            console.log(err);
            alert("error "+err.reason);
          }
          else{
            Router.go('radio');
          }
        });
    },
    'submit .login-form': function(e,t){
        e.preventDefault();
        var logData = {};
        logData.username = $('.login-form [name=username]').val();
        logData.password = $('.login-form [name=password]').val();
        Meteor.loginWithPassword(logData.username, logData.password, function(err){
          if(err){
            console.log(err);
            alert("error "+err.reason);
          }
          else{
            Router.go('radio');
          }
        });
    }
});
