
msgsHideTimeout = null;
recountMax = true;
lastDate = 0;
var heartTimer = null;
var lastRadioName = null;

function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}


Template.radio.helpers({
    curRadio: function(){

      var radio = Radios.findOne(Radio.curRadioId.get());

      if(radio){
        if(radio.name !== lastRadioName){
          $(".str_origin").html(radio.name);
          $('#marquee').liMarquee('update');
          lastRadioName = radio.name;
        }


        var user = Meteor.user();
        //console.log(user)
        if(user && user.loginStateSignedUp){
          var fav = user.profile.favRadios
          var pos = fav.indexOf(radio._id);
          if(pos != -1){
            radio.fav = true;
          }
          else{
            radio.fav = false;
          }
        }
      }

      if(radio && Radio.curRadioLike !== radio.like){
        $('#heartCounter').attr('src', 'img/heart.gif');
        Radio.curRadioLike = radio.like;
        if(!heartTimer){
          heartTimer = window.setTimeout(function(){
            $('#heartCounter').attr('src', 'img/heart.png');
            heartTimer = null;
          }, 1000);
        }
      }

      return radio;
    },
    favRadios: function(){
        var ret = [];
        var user = Meteor.user();
        if(user && user.loginStateSignedUp){
            var radios = Radios.find({genre: Radio.curRadioGenre}).fetch();

            var fav = user.profile.favRadios;
            var curRadioId = Radio.curRadioId.get();
            ret = _.filter(radios, function(val){
                if(curRadioId == val._id){
                    val.active = true;
                }
                if(fav && fav.indexOf(val._id) != -1){
                    return true;
                }
                else{
                    return false;
                }
            });
        }
        return ret;
    },
    genres: function(){
      var ret = [];
      var user = Meteor.user();
      var fav = null;
      if(user){
        fav = user.profile.favGenres
      }
      ret = _.sortBy(Genres.find().fetch(), function(val){
        if(fav && fav.indexOf(val._id) != -1){
          val.fav = true;
        }
        else{
          val.fav = false;
        }
        return 0;
      })
      return ret;
    },
    favGenres: function(){
      var ret = [];
      var user = Meteor.user();
      var fav = null;
      if(user){
        fav = user.profile.favGenres
      }

      ret = _.filter(Genres.find().fetch(), function(val){
        if(fav && fav.indexOf(val._id) != -1){
          return true;
        }
        else if(fav == null){
          return true;
        }
        else{
          return false;
        }
      });
      return ret.splice(0, 8);
    },
    messages: function(){
      if(/*Radio.curRadioId.get()*/ 1){
        var ret, retA, msgs = [];
        if(!lastDate){
          lastDate = 0;
        }
        msgs = Messages.find().fetch();
        console.log('msgs', msgs)
        var maxDate = _.max(msgs, function(val){return val.date;}).date;
        ret = _.filter(msgs, function(val){
          if(val.date > lastDate){
            return true;
          }
          else{
            return false;
          }
        });


        if(/*Meteor.user() && Meteor.user().profile.showMessages*/1){
          for(var i=0,l=ret.length; i<l; i++){
            showMessage(ret[i]);
          }
        }
        if(ret.length){
          if(!msgsHideTimeout){
            msgsHideTimeout = window.setTimeout(function(){
              hideMessage();
            },10000);
          }
        }
        lastDate = maxDate;
      }
    },
    avatar: function(){
      var u = Meteor.user();
      var img = "/img/default-avatar.png";
      if(u && u.profile.image){
        img = u.profile.image;
      }
      return img;
    }
});
Template.radio.onCreated(function(){
    var self = this;
    self.autorun(function(){
      var radio = Radio.curRadioId.get();
      self.subscribe("messages", radio);

    });
    window.wid = guid();
    self.subscribe("disconnect", window.wid);
});
Template.radio.onRendered(function () {
  $('#marquee').liMarquee({scrollamount:"20", circular:false});
});

function hideMessage(){
  msgsHideTimeout = null;
  if($(".message").length != 0 ){
      $(".message:first-child").fadeOut(500,function()
      {
          $(this).remove();
      });
      if(!msgsHideTimeout){
        msgsHideTimeout = window.setTimeout(function(){
          hideMessage();
        },10000);
      }
  }
}
function showAdminMessage(data){
  console.log('show admin', data)
}
function showMessage(data){
  var time=0;
  var k=$(".message").length;
  var $el = $('.bro-header');
  var bottom = $el.offset().top + $el.outerHeight(true);
  var bottom_2=9999999;


  top=$(".main_chat").offset().top;
  if (k>=1){
      bottom_2 = $(".message:nth-child(1)").offset().top + $(".message:nth-child(1)").outerHeight(true);
  }

  $(" <div class='message'  ><div><img class='prof' src='"+ data.avatar +"'/></div>" +
      "<div><div class='info'> <!--<a href=''><img class='fb_mess'src='img/fb.png'></a>--> 	<span>"+
      data.username+"</span> <img class='cross' src='img/cross.png'></div> <div class='main_mess'>"+
      data.message+" </div></div> </div>").appendTo(".content");
  $(".message:nth-child("+(k+1)+")").hide();
  $(".message:nth-child("+(k+1)+")").slideDown(500);

  if (bottom_2>bottom){
      k+=1;
  }
  else  {
      $(".message:first-child").fadeOut(0,function(){
          $(this).remove();
      });
  }
  k=$(".message").length;

}
Template.radio.events({
    'click .bro-mustype': function(e,t){
        e.preventDefault();
        var newGenre = $(e.currentTarget).attr('href');
        if(Radio.curRadioGenre != newGenre){
          var olrR = Radio.curRadioId.get();
          var r = Radio.getNextBest(newGenre);
          Player.play(r.url);
          Meteor.call('incRadioListeners', olrR, r._id);
          Meteor.call('getRadioInfo', r._id, r.url);
          if(Radio.updateInfoInterval == null){
            Radio.updateInfoInterval = window.setInterval(function(){
              if(Radio.updateInfoInterval != null){
                var r = Radios.findOne(Radio.curRadioId.get());
                Meteor.call('getRadioInfo', r._id, r.url);
              }
              else{
                window.clearInterval(this)
              }

            },5000);
          }
          $('.bro-mustype--active .bro-mustype-favRadios').css('display', 'none');
          $('.bro-mustype--active .bro-mustype-discript').css('display', 'none');
          $(".bro-mustype--active").removeClass("bro-mustype--active");
          $(e.currentTarget).addClass("bro-mustype--active");
          $('.bro-mustype--active .bro-mustype-discript').css('display', 'table');
        }

    },
    'click .bro-mustype .bro-mustype-favRadios a': function(e, t){
        e.preventDefault();
        var olrR = Radio.curRadioId.get();
        var r = Radio.getById($(e.currentTarget).attr('href'));
        Player.play(r.url);
        Meteor.call('incRadioListeners', olrR, r._id);
        Meteor.call('getRadioInfo', r._id, r.url);
        if(Radio.updateInfoInterval == null){
          Radio.updateInfoInterval = window.setInterval(function(){
            if(Radio.updateInfoInterval != null){
              var r = Radios.findOne(Radio.curRadioId.get());
              Meteor.call('getRadioInfo', r._id, r.url);
            }
            else{
              window.clearInterval(this)
            }

          },5000);
        }
    },
    'click #likeBtn': function(e,t){
        e.preventDefault();
        var d = new Date();
        var time = d.getHours() + ':' + d.getMinutes();
        if(Radio.lastLike.time && Radio.lastLike.time !== time ){
          Radio.lastLike.counter = 0;
        }
        if(Radio.lastLike.counter < 60){
          Radio.lastLike = {time:time, counter:++Radio.lastLike.counter};
          Meteor.call('likeRadio', Radio.curRadioId.get());
        }

    },
    'click #logout': function(event){
        event.preventDefault();
        $("#megawarpper .bro-header").removeClass("blur");
        $("#megawarpper .bro-main").removeClass("blur");
        $("#megawarpper .bro-player").removeClass("blur");
        $("#main_chat").removeClass("moved");
        $(".bro-sidepanel").addClass("bro-sidepanel--close");
        Meteor.logout();
    },
    'click .bro-settings': function(e,t){
        e.preventDefault();
        $("#megawarpper .bro-header").addClass("blur");
        $("#megawarpper .bro-main").addClass("blur");
        $("#megawarpper .bro-player").addClass("blur");
        $("#main_chat").addClass("moved");
        $(".bro-sidepanel").removeClass("bro-sidepanel--close");
    },
    'click .bro-sidepanel-close': function(e,t){
        e.preventDefault();
        $("#megawarpper .bro-header").removeClass("blur");
        $("#megawarpper .bro-main").removeClass("blur");
        $("#megawarpper .bro-player").removeClass("blur");
        $("#main_chat").removeClass("moved");
        $(".bro-sidepanel").addClass("bro-sidepanel--close");
    },
    'click .bro-sidepanel-mustype-item a': function(e,t){
        e.preventDefault();
        var id = $(e.currentTarget).attr('href');
        var pos = Meteor.user().profile.favGenres.indexOf(id);
        if(pos != -1){
          Meteor.users.update({"_id": Meteor.userId()}, {$pull: {"profile.favGenres": id}});
        }
        else if(Meteor.user().profile.favGenres.length < 8){
          Meteor.users.update({"_id": Meteor.userId()}, {$push: {"profile.favGenres": id}});
        }
    },
    'click #next': function(event){
        event.preventDefault();
        var olrR = Radio.curRadioId.get();
        var r = Radio.getNextBest(Radio.curRadioGenre);
        Player.play(r.url);
        Meteor.call('incRadioListeners', olrR, r._id);
        Meteor.call('getRadioInfo', r._id, r.url);
    },
    'click #prev': function(event){
        event.preventDefault();
        var olrR = Radio.curRadioId.get();
        var r = Radio.getPrev();
        Player.play(r.url);
        Meteor.call('incRadioListeners', olrR, r._id);
        Meteor.call('getRadioInfo', r._id, r.url);
    },
    'click .pause': function(event){
        event.preventDefault();
        Player.pause();
    },
    'click .radio_fav': function(e){
        e.stopPropagation()
        var id = Radio.curRadioId.get();
        if(id){
          var pos = Meteor.user().profile.favRadios.indexOf(id);
          if(pos != -1){
            Meteor.users.update({"_id": Meteor.userId()}, {$pull: {"profile.favRadios": id}});
          }
          else{
            Meteor.users.update({"_id": Meteor.userId()}, {$push: {"profile.favRadios": id}});
          }
        }
    },
    'click #radio_fav_big': function(e){
        e.preventDefault();
        var favRadios_elem = $(e.currentTarget).parent().prev();
        var descr_elem = favRadios_elem.prev();

        if(favRadios_elem.css('display') == 'none'){
            descr_elem.css('display', 'none');
            favRadios_elem.css('display', 'block');
        }
        else{
            favRadios_elem.css('display', 'none');
            descr_elem.css('display', 'table')
        }
    },
    'submit .bro-form-massage': function(event){
        event.preventDefault();
        var inpElem = $(".bro-form-massage input");
        var msg = inpElem.val();
        if (msg == ""){
          $(".bro-form-massage input").css("border","3px solid red");
        }
        else {
          //Првоерка длины сообщения
          if (msg.length>120){
              msg = msg.substr(0,119);
              msg = msg + "...";
          }
          inpElem.val("").css("border","none");
          if(Radio.curRadioId.get()){
            var img = Meteor.user().profile.image;
            if(!img){
              img = "/img/default-avatar.png";
            }
            var data = {
              radio: Radio.curRadioId.get(),
              message: msg,
              username: (Meteor.user().username.indexOf('guest-#') == 0) ? "Guest" : Meteor.user().profile.fullname,
              avatar: img
            }
            Meteor.call('sendMessage', data);
          }
        }

        //showMessage();
    },
    'click .bro-swith': function(event){
      $(event.currentTarget).toggleClass("bro-swith--on");
    },
    'click #sortPopular': function(event){
      event.preventDefault();
      var status = false;
      if($(event.currentTarget).hasClass("bro-swith--on")){
        status = true;
      }
      Meteor.users.update({"_id": Meteor.userId()}, {$set: {"profile.sortPopular": status, "profile.sortFavorite": false}});
    },
    'click #sortFavorite': function(event){
      event.preventDefault();
      var status = false;
      if($(event.currentTarget).hasClass("bro-swith--on")){
        status = true;
      }
      Meteor.users.update({"_id": Meteor.userId()}, {$set: {"profile.sortFavorite": status, "profile.sortPopular": false}});
    },
    'click #toggleMessages': function(event){
      event.preventDefault();
      var status = false;
      if($(event.currentTarget).hasClass("bro-swith--on")){
        status = true;
      }
      Meteor.users.update({"_id": Meteor.userId()}, {$set: {"profile.showMessages": status}});
    }
});
