Template.radiosList.helpers({
    radiosList: function (){
      return Radios.find({});
    },
    genresList: function (){
      return Genres.find({});
    },
    genreName:function(id){
      if(Genres.findOne(id)){
        return Genres.findOne(id).name;
      }
      else{
        return '';
      }
    }
});

Template.genresList.helpers({
    genresList: function (){
      return Genres.find({});
    },
    genre: function(){
      return genre;
    }
});

Template.usersList.helpers({
    usersList: function (){
      var ret = _.filter(Meteor.users.find({}).fetch(), function(val){
        if(val.username.indexOf('guest-#') == 0){
          return false;
        }
        else{
          return true;
        }
      });
      return ret;
    }
});

Template.radiosList.events({
    'click #addRadioBtn': function(e,t){
        e.preventDefault();
    },
    'click #radiosBtn': function(e,t){
        e.preventDefault();

        var radio = {
          _id:$('#radioId').val(),
          name:$('#radioName').val(),
          description:$('#radioDescription').val(),
          url:$('#radioUrl').val(),
          genre:$('#radioGenre').val()
        };
        Meteor.call('addEditRadio', radio);
        $('#addRadioModal').modal('hide');
        $('#radioId').val('');
        $('#radioName').val('');
        $('#radioUrl').val('');
        $('#radioDescription').val('');
        $('#RadioGenre').val('');
    },
    'click .radioDelBtn': function(e,t){
        e.preventDefault();
        var id = $(e.currentTarget).data('id');
        Meteor.call('delRadio', id);
    },
    'dblclick tr': function(e,t){
        e.preventDefault();
        var id = $(e.currentTarget).data('id');
        var radio = Radios.findOne(id)

        $('#radioId').val(radio._id);
        $('#radioName').val(radio.name);
        $('#radioUrl').val(radio.url);
        $('#radioDescription').val(radio.description);
        $('#radioGenre').val(radio.genre);

        $('#addRadioModal').modal('show');
    }
});
Template.genresList.events({
    'click #genresBtn': function(e,t){
        e.preventDefault();
        var genre = {
          _id:$('#genreId').val(),
          name:$('#genreName').val(),
          icon:$('#genreIcon').val()
        };
        Meteor.call('addEditGenre', genre);
        $('#addGenreModal').modal('hide');
        $('#genreId').val('');
        $('#genreName').val('');
        $('#genreIcon').val('');
    },
    'click .genreDelBtn': function(e,t){
        e.preventDefault();
        var id = $(e.currentTarget).data('id');
        Meteor.call('delGenre', id);
    },
    'dblclick tr': function(e,t){
        e.preventDefault();
        var id = $(e.currentTarget).data('id');
        var genre = Genres.findOne(id)

        $('#genreId').val(genre._id);
        $('#genreName').val(genre.name);
        $('#genreIcon').val(genre.icon);

        $('#addGenreModal').modal('show');
    }
});

Template.adminHeader.events({
    'click #cleareRadiosBtn': function(e,t){
        e.preventDefault();
        Meteor.call('cleareRadios');
    },
    'click #initRadiosBtn': function(e,t){
        e.preventDefault();
        Meteor.call('initRadios');
    },
    'click #cleareDopinfoBtn': function(e,t){
        e.preventDefault();
        Meteor.call('cleareDopinfo');
    }
});

Template.msgToUsers.events({
    'click #sendBtn': function(e,t){
        e.preventDefault();
        var data = {
          radio: 0,
          message: $('#msg').val(),
          avatar: "/img/default-avatar.png",
          username: "Admin"
        }
        Meteor.call('sendMessage', data);
    }
});
