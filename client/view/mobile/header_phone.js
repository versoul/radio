Template.header_phone.helpers({
    avatar: function(){
      var u = Meteor.user();
      var img = "/img/default-avatar.png";
      if(u && u.profile.image){
        img = u.profile.image;
      }
      return img;
    }
});
Template.header_phone.events({
    'click #btnSettings': function(e){
        e.preventDefault();
        var menu_elem = $('#menu');

        if(menu_elem.css('top') == '0px'){
            menu_elem.animate({
                top: '-100%'
            }, 500);
        }
        else{
            menu_elem.animate({
                top: 0
            }, 500);
        }
    },
    'click #logout': function(event){
        event.preventDefault();
        Meteor.logout();
    }
});
