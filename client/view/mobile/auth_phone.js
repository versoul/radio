

Template.auth_phone.onCreated(function(){
    $('body').css('height', window.innerHeight+'px')
});
Template.auth_phone.events({
    'click #btnRegistration': function(e,t){
        e.preventDefault();
        var regData = {};
        regData.fullname = $('.register-form [name=fullname]').val();
        regData.email = $('.register-form [name=email]').val();
        regData.address = $('.register-form [name=address]').val();
        regData.city = $('.register-form [name=city]').val();
        regData.country = $('.register-form [name=country]').val();
        regData.username = $('.register-form [name=username]').val();
        regData.password = $('.register-form [name=password]').val();

        Accounts.createUser({
          username: regData.username,
          email: regData.email,
          password: regData.password,
          profile: {
            fullname: regData.fullname,
            country: regData.country,
            city: regData.city,
            address: regData.address,
            favGenres:[],
            favRadios:[],
            showMessages:true,
            sortPopular:false,
            sortFavorite:false
          }
        }, function(err){
          if(err){
            console.log(err);
            alert("error "+err.reason);
          }
          else{
            Router.go('radio');
          }
        });
    },
    'click #btnLogin': function(e){
        e.preventDefault();
        var logData = {};
        logData.username = $('.login-form [name=username]').val();
        logData.password = $('.login-form [name=password]').val();
        Meteor.loginWithPassword(logData.username, logData.password, function(err){
          if(err){
            console.log(err);
            alert("error "+err.reason);
          }
          else{
            Router.go('radio');
          }
        });
    },
    'click .logregToggle': function(e){
        e.preventDefault();
        var loginForm = $('.login-form');
        var registerForm = $('.register-form');
        if(registerForm.css('display') == 'none'){
            loginForm.css('display', 'none');
            registerForm.css('display', 'block');
        }
        else{
            registerForm.css('display', 'none');
            loginForm.css('display', 'block');
        }
    }
});
