Template.radio_phone.helpers({
    genres: function(){
      var ret = [];
      var user = Meteor.user();
      var fav = null;
      if(user){
        fav = user.profile.favGenres
      }
      ret = _.sortBy(Genres.find().fetch(), function(val){
        if(fav && fav.indexOf(val._id) != -1){
          val.fav = true;
        }
        else{
          val.fav = false;
        }
        return 0;
      })
      return ret;
  },
  favGenres: function(){
    var ret = [];
    var user = Meteor.user();
    var fav = null;
    if(user){
      fav = user.profile.favGenres
    }
    ret = _.filter(Genres.find().fetch(), function(val){
      if(fav && fav.indexOf(val._id) != -1){
        return true;
      }
      else if(fav == null){
        return true;
      }
      else{
        return false;
      }
    });
    return ret.splice(0, 8);
  }
});
Template.radio_phone.onCreated(function(){
    $('body').css('height', window.innerHeight+'px')
});
