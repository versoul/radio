
var lastRadioName = null;
function guid() {//TODO вынести в одно место
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}
Template.listen_phone.helpers({
    curRadio: function(){

      var radio = Radios.findOne(Radio.curRadioId.get());

      if(radio){
        if(radio.name !== lastRadioName){
          $(".str_origin").html(radio.name);
          $('#marquee').liMarquee('update');
          lastRadioName = radio.name;
          console.log('radio changed')
      }


        var user = Meteor.user();
        //console.log(user)
        if(user && user.loginStateSignedUp){
          var fav = user.profile.favRadios
          var pos = fav.indexOf(radio._id);
          if(pos != -1){
            radio.fav = true;
          }
          else{
            radio.fav = false;
          }
      }
      }

      /*if(radio && Radio.curRadioLike !== radio.like){
        $('#heartCounter').attr('src', 'img/heart.gif');
        Radio.curRadioLike = radio.like;
        if(!heartTimer){
          heartTimer = window.setTimeout(function(){
            $('#heartCounter').attr('src', 'img/heart.png');
            heartTimer = null;
          }, 1000);
        }
    }*/
      return radio;
    },
    genres: function(){
      console.log('gg',Router.current().params._id)
      return 0;
    },
    curGenre: function(){
        return Genres.findOne(Router.current().params._id);
    },
    favRadios: function(){
        var ret = [];
        var user = Meteor.user();
        if(user && user.loginStateSignedUp){
            var radios = Radios.find().fetch();
            var fav = user.profile.favRadios;
            var curRadioId = Radio.curRadioId.get();
            ret = _.filter(radios, function(val){
                if(curRadioId == val._id){
                    val.active = true;
                }
                if(fav && fav.indexOf(val._id) != -1){
                    return true;
                }
                else{
                    return false;
                }
            });
        }
        return ret;
    },
    messages: function(){
        var msgs = Messages.find({}, {limit:10}).fetch();
        console.log('msgs', msgs)
        return msgs;
    },
    avatar: function(){
      var u = Meteor.user();
      var img = "/img/default-avatar.png";
      if(u && u.profile.image){
        img = u.profile.image;
      }
      return img;
    }
});
Template.listen_phone.onCreated(function(){
    var self = this;
    self.autorun(function(){
      var radio = Radio.curRadioId.get();
      self.subscribe("messages", radio);

    });
    window.wid = guid();
    self.subscribe("disconnect", window.wid);
    $('body').css('height', window.innerHeight+'px')
});
Template.listen_phone.onRendered(function(){
    $('#marquee').liMarquee({scrollamount:"20", circular:false});
    Radio.curRadioGenre = Router.current().params._id;

    //autoplay
    function autoplay(){
        if($('#btn_play')){
            $('#btn_play').click();
        }
        else{
            setTimeout(autoplay, 1000);
        }

    }
    //setTimeout(autoplay, 1000);
});
Template.listen_phone.events({
    'click #btn_play': function(e,t){
        e.preventDefault();
        var img = $(e.currentTarget).children();
        if(img.attr('src') == '/img/phone_btn_play.png'){//play
            var oldR = Radio.curRadioId.get();
            var r = null;
            if(oldR){
                Player.resume();
            }
            else{
                r = Radio.getNextBest(Router.current().params._id);
                Player.play(r.url);

                Meteor.call('incRadioListeners', oldR, r._id);
                Meteor.call('getRadioInfo', r._id, r.url);
                if(Radio.updateInfoInterval == null){
                    Radio.updateInfoInterval = window.setInterval(function(){
                        if(Radio.updateInfoInterval != null){
                            var r = Radios.findOne(Radio.curRadioId.get());
                            Meteor.call('getRadioInfo', r._id, r.url);
                        }
                        else{
                            window.clearInterval(this)
                        }
                    },5000);
                }
            }

            img.attr('src', '/img/phone_btn_pause.png');
        }
        else{//pause
            Player.pause();
            img.attr('src', '/img/phone_btn_play.png');
        }
    },
    'click #btn_next': function(event){
        event.preventDefault();
        var olrR = Radio.curRadioId.get();
        var r = Radio.getNextBest(Radio.curRadioGenre);
        Player.play(r.url);
        Meteor.call('incRadioListeners', olrR, r._id);
        Meteor.call('getRadioInfo', r._id, r.url);
    },
    'click #btn_prev': function(event){
        event.preventDefault();
        var olrR = Radio.curRadioId.get();
        var r = Radio.getPrev();
        Player.play(r.url);
        Meteor.call('incRadioListeners', olrR, r._id);
        Meteor.call('getRadioInfo', r._id, r.url);
    },
    'click .pause': function(event){
        event.preventDefault();
        Player.pause();
    },
    'click #btn_like': function(e,t){
        e.preventDefault();
        var d = new Date();
        var time = d.getHours() + ':' + d.getMinutes();
        if(Radio.lastLike.time && Radio.lastLike.time !== time ){
          Radio.lastLike.counter = 0;
        }
        if(Radio.lastLike.counter < 60){
          Radio.lastLike = {time:time, counter:++Radio.lastLike.counter};
          Meteor.call('likeRadio', Radio.curRadioId.get());
        }

    },
    'click #btn_fav': function(e){
        e.preventDefault();
        var user = Meteor.user();
        if(user && user.loginStateSignedUp){
            var id = Radio.curRadioId.get();
            if(id){
              var pos = user.profile.favRadios.indexOf(id);
              if(pos != -1){
                Meteor.users.update({"_id": Meteor.userId()}, {$pull: {"profile.favRadios": id}});
              }
              else{
                Meteor.users.update({"_id": Meteor.userId()}, {$push: {"profile.favRadios": id}});
              }
            }
        }
    },
    'click #btn_toggle_fav': function(e){
        e.preventDefault();
        var genre_box = $('.genre_content .description');
        var fav_box = $('.genre_content .fav');
        if(genre_box.css('display') == 'inline-block'){
            genre_box.css('display', 'none');
            fav_box.css('display', 'block');
        }
        else{
            fav_box.css('display', 'none');
            genre_box.css('display', 'inline-block');
        }
    },
    'focus #message_input, focusout #message_input': function(e){
        console.log('focusss');
        var box_elem = $('#messages_box');

        if(box_elem.css('display') == 'block'){
            $('.wrapper').removeClass('blur');
            box_elem.css('opacity', '');
            box_elem.css('display', 'none');

        }
        else{
            $('.wrapper').addClass('blur');
            box_elem.css('display', 'block');
            box_elem.css('opacity', '1');
        }
    },
    'submit #message_form': function(e){
        e.preventDefault();
        if(Radio.curRadioId.get()){
          var img = Meteor.user().profile.image;
          if(!img){
            img = "/img/default-avatar.png";
          }
          var data = {
            radio: Radio.curRadioId.get(),
            message: $('#message_input').val(),
            username: (Meteor.user().username.indexOf('guest-#') == 0) ? "Guest" : Meteor.user().profile.fullname,
            avatar: img
          }
          Meteor.call('sendMessage', data);
          $('#message_input').val('');
      }
    }
});
