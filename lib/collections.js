Radios = new Mongo.Collection('radios');
Radios.allow({
    'insert': function (userId,doc) {
        return true;
    },
    'remove': function (userId,doc) {
        return true;
    },
    'update': function (userId,doc) {
        return true;
    }
});

Genres = new Mongo.Collection('genres');
Genres.allow({
    'insert': function (userId,doc) {
        return true;
    },
    'remove': function (userId,doc) {
        return true;
    },
    'update': function (userId,doc) {
        return true;
    }
});

RadioGroups = new Mongo.Collection('radioGroups');
RadioGroups.allow({
    'insert': function (userId,doc) {
        return true;
    },
    'remove': function (userId,doc) {
        return true;
    },
    'update': function (userId,doc) {
        return true;
    }
});

Messages = new Mongo.Collection('messages');
Messages.allow({
    'insert': function (userId,doc) {
        return true;
    },
    'remove': function (userId,doc) {
        return true;
    },
    'update': function (userId,doc) {
        return true;
    }
});

Disconnect = new Mongo.Collection('disconnect');
Disconnect.allow({
    'insert': function (userId,doc) {
        return true;
    },
    'remove': function (userId,doc) {
        return true;
    },
    'update': function (userId,doc) {
        return true;
    }
});
RadioOnline = new Mongo.Collection('radioOnline');
