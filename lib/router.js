Router.configure({
    layoutTemplate: 'layout'
});

Router.map(function() {
    this.route('radio', {
        path: '/',
        template: 'radio',
        title:'Радио'
    });
    this.route('listen', {
        path: '/listen/:_id',
        template: 'listen',
        title:''
    });
    this.route('auth', {
        path: '/auth',
        template: 'auth',
        title:''
    });
    this.route('register', {
        layoutTemplate: 'minLayout',
        path: '/register',
        template: 'register',
        title:'Auth',
        onBeforeAction: function() {
          $('body').addClass('login');
          this.next();
        },

        onStop: function() {
          $('body').removeClass('login');
        }
    });


    this.route('admin', {
        layoutTemplate: 'adminLayout',
        path: '/admin',
        template: 'admin',
        title:'Админка'
    });
    this.route('radiosList', {
        layoutTemplate: 'adminLayout',
        path: '/radiosList',
        template: 'radiosList',
        title:'Станции'
    });
    this.route('genresList', {
        layoutTemplate: 'adminLayout',
        path: '/genresList',
        template: 'genresList',
        title:'Жанры'
    });
    this.route('usersList', {
        layoutTemplate: 'adminLayout',
        path: '/usersList',
        template: 'usersList',
        title:'Пользователи'
    });
    this.route('msgToUsers', {
        layoutTemplate: 'adminLayout',
        path: '/msgToUsers',
        template: 'msgToUsers',
        title:'Сообщение пользователям'
    });
});
