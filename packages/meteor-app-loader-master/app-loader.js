if (Meteor.isServer) {
	Inject.rawHead("loader", Assets.getText('loader.html'));
}

if (Meteor.isClient) {
	Meteor.startup(function() {
		setTimeout(function() {
			$("#inject-loader-wrapper .showbox").fadeOut(500, function() { $(this).parent().remove(); });
		}, 500);
	});
}
